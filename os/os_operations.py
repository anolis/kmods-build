#!/usr/bin/env python3

import yaml
import os

class os_mapping_entry:
        def __init__(self, derived_os_version, derived_kernel_version, derived_vmlinuz_md5sum, anolis_os_version, anolis_kernel_version):
                self.derived_os_version=derived_os_version
                self.derived_kernel_version=derived_kernel_version
                self.derived_vmlinuz_md5sum=derived_vmlinuz_md5sum
                self.anolis_os_version=anolis_os_version
                self.anolis_kernel_version=anolis_kernel_version

        def to_dict(self):
                return {
                        'derived_os_version':self.derived_os_version,
                        'derived_kernel_version':self.derived_kernel_version,
                        'derived_vmlinuz_md5sum':self.derived_vmlinuz_md5sum,
                        'anolis_os_version':self.anolis_os_version,
                        'anolis_kernel_version':self.anolis_kernel_version
                }

database=os.path.abspath(os.path.join(os.path.dirname(__file__),'database/os_mapping.yaml'))

# query: Query the mapping relationship between the derivative version and anolis
#
# Parameters:
#       os_version: The version of the derivative OS
#       kernel_version: The version of the derivative kernel
#       vmlinuz_md5sum: The md5sum of the derivative kernel
#
# Return:
#       0, [anolis_os, anolis_kernel] : found the mapping relationship
#       1, None: not found the mapping relationship
#       2, None: found the mapping relationship but the md5sum is not match
#       -1, None: os_mapping.yaml does not exist
#       -2, None: os_mapping.yaml is empty

def query(os_versoin, kernel_version, vmlinuz_md5sum):
        if not os.path.exists(database):
                print('[Error] os_mapping.yaml does not exist')
                return -1, None
        with open(database, 'r', encoding='utf-8') as file:
                mapping = yaml.safe_load(file)
        if mapping is None:
                print('[Error] os_mapping.yaml is empty')
                return -2, None
        else:
                for item in mapping:
                        if item['derived_os_version'] == os_versoin and item['derived_kernel_version'] == kernel_version:
                                if item['derived_vmlinuz_md5sum'] == vmlinuz_md5sum:
                                        return 0, [item['anolis_os_version'], item['anolis_kernel_version']]
                                else:
                                        return 2, None
                return 1, None

def update(derived_os_version, derived_os_kernel_version, derived_vmlinuz_md5sum, anolis_os_version, anolis_kernel_version):
        entry = os_mapping_entry(derived_os_version, derived_os_kernel_version, derived_vmlinuz_md5sum, anolis_os_version, anolis_kernel_version)

        if not os.path.exists(database):
                print('[Error] os_mapping.yaml does not exist')
                return -1

        with open(database, 'r', encoding='utf-8') as file:
                mapping = yaml.safe_load(file)
        if mapping is None:
                mapping = []
        else:
                for item in mapping:
                        if entry.to_dict() == item:
                                print('[Error] duplicate entry: {}'.format(entry.to_dict()))
                                return -2

        mapping.append(entry.to_dict())

        with open(database, 'w', encoding='utf-8') as file:
                yaml.dump(mapping, file)

        return 0

def remove(derived_os_version, derived_os_kernel_version, derived_vmlinuz_md5sum, anolis_os_version, anolis_kernel_version):
        entry = os_mapping_entry(derived_os_version, derived_os_kernel_version, derived_vmlinuz_md5sum, anolis_os_version, anolis_kernel_version)

        if not os.path.exists(database):
                print('[Error] os_mapping.yaml does not exist')
                return -1
        with open(database, 'r', encoding='utf-8') as file:
                mapping = yaml.safe_load(file)
        if mapping is None:
                print('[Error] os_mapping.yaml is empty')
                return -2
        else:
                for item in mapping:
                        if entry.to_dict() == item:
                                found = True
                                break
                if found:
                        mapping.remove(item)
                else:
                        print('[Error] entry not found: {}'.format(entry.to_dict()))
                        return -3

        with open(database, 'w', encoding='utf-8') as file:
                yaml.dump(mapping, file)

        return 0

if __name__ == '__main__':
        update('kos 5.8sp2', '5.10.134-17.2.2.kos5.x86_64', "27f514ef26fc45acc0a36b4aa5eaeb5c", 'Anolis OS 8.9', '5.10.134-17.2.an8.x86_64')
        update("Alibaba Cloud Linux 3.2104 U12 (Pro Edition)", "5.10.134-010.ali5000.pro.al8.aarch64", "e572109cafa86df4161e821d9d39487b", 'Anolis OS 8.9', '5.10.134-17.an8.aarch64')
        update("Alibaba Cloud Linux 3.2104 U12 (Pro Edition)", "5.10.134-010.ali5000.pro.al8.x86_64", "ff37bfc412daf439252e244d442faab2", 'Anolis OS 8.9', '5.10.134-17.an8.x86_64')
        errcode, output = query('kos 5.8sp2', '5.10.134-17.2.2.kos5.x86_64', "27f514ef26fc45acc0a36b4aa5eaeb5c")
        print(errcode)
        print(output)

        errcode, output = query("Alibaba Cloud Linux 3.2104 U12 (Pro Edition)", "5.10.134-010.ali5000.pro.al8.aarch64", "e572109cafa86df4161e821d9d39487b")
        print(errcode)
        print(output)

        errcode, output = query("Alibaba Cloud Linux 3.2104 U12 (Pro Edition)", "5.10.134-010.ali5000.pro.al8.aarch64", "ff37bfc412daf439252e244d442faab2")
        print(errcode)
        print(output)