#!/usr/bin/env python3

import os
import shutil
import traceback

import yaml
import rpm
import requests
import subprocess
from bs4 import BeautifulSoup
import re

# oot_info_group: {'anolis_os_version':xx, 'anolis_kernel_version':xx, 'oot_modules':list()}

# oot_info_entry: {'oot_name':xx, 'oot_version':xx, 'arch':set(), 'modules':list()}

# module_info_entry: {'name':xx, 'version':xx, 'srcversion':list()}

database_path=os.path.abspath(os.path.join(os.path.dirname(__file__),'database'))
database=os.path.join(database_path,'oot_baseline.yaml')
buffer_path=os.path.join(database_path,'buffer')

# query: Query whether the OOT driver modules has been recorded.
#
# parameters:
#       anolis_os_version: xx
#       anolis_kernel_version: xx
#       arch: aarch64/x86_64
#       oot_list: 2d list, each item is a list of [module_name, module_version, module_srcversion]
#
# return:
#       0, None: all modules are found
#       1, [[oot_list, error_description]: some modules are not found
#               error_description:
#                       'Module Not Found'
#                       'srcversion Not Match: xx'
#                       'Module Version Not Match: xx'
#                       'Arch Not Match: xx'
#                       'OS/Kernel Version Not Match'
#       -1, None: oot_baseline.yaml does not exist
#       -2, None: oot_baseline.yaml is empty
#       -3, None: oot_list is not a 2d list
#       -4, None: oot_list is empty

def query(anolis_os_version, anolis_kernel_version, arch, oot_list):
        ret = []
        group_found = False
        module_found = False

        if not os.path.exists(database):
                print('[Error] oot_baseline.yaml does not exist')
                return -1, None

        with open(database, 'r', encoding='utf-8') as file:
                oot_baseline = yaml.safe_load(file)
        if oot_baseline is None:
                print('[Error] oot_baseline.yaml is empty')
                return -2, None

        if arch is None:
                print('[Warning] not assign arch')

        if not is_2d_list(oot_list):
                print('[Error] oot_list MUST be 2d list')
                return -3, None
        if len(oot_list) == 0:
                print('[Error] oot_list is empty')
                return -4, None

        for group in oot_baseline:
                if group['anolis_os_version'] == anolis_os_version:
                        if (compare_kernel_version(group['anolis_kernel_version'], anolis_kernel_version)):
                                group_found = True
        if group_found:
                for module in oot_list:
                        dump_oot = None
                        dump_module_index = -1
                        for oot in group['oot_modules']:
                                errcode, module_index = oot_query_module(oot, {'name':module[0], 'version':module[1], 'srcversion':[module[2]]})
                                if errcode <= 0 and errcode >= -1:
                                        break
                                if errcode == -2 and dump_module_index == -1:
                                        dump_module_index = module_index
                                        dump_oot = oot

                        if errcode <= -2:
                                if dump_module_index >= 0:
                                        ret.append([module, 'Module Version Not Match: {} <-> {}'.format(module[1], dump_oot['modules'][dump_module_index]['version'])])
                                else:
                                        ret.append([module, 'Module Not Found'])
                        elif errcode == -1:
                                        ret.append([module, 'srcversion Not Match: {}'.format(oot['modules'][module_index]['srcversion'])])
                        else:
                                if arch and arch not in oot['arch']:
                                        ret.append([module, 'Arch Not Match: {} <-> {}'.format(arch, oot['arch'])])

        else:
                for module in oot_list:
                        ret.append([module, 'OS/Kernel Version Not Match'])

        if len(ret) == 0:
                return 0, None
        else:
                return 1, ret

def update(anolis_os_version, anolis_kernel_version, filepath, is_url=True):
        try:
                if not os.path.exists(database):
                        print('[Error] oot_baseline.yaml does not exist')
                        return -1
                with open(database, 'r', encoding='utf-8') as file:
                        oot_baseline = yaml.safe_load(file)
                if oot_baseline is None:
                        oot_baseline = []

                if is_url:
                        links = list_koji_packages(filepath)
                        packages = []
                        for link in links:
                                packages.append(link.split('/')[-1])
                else:
                        if filepath.endswith('.rpm') and not filepath.endswith('.src.rpm'):
                                links = [filepath]
                                packages = [filepath.split('/')[-1]]
                        else:
                                print('[Error] {} is not a rpm file'.format(filepath))
                                return -2

                packages_buffer = []
                for i in range(len(packages)):
                        packages_buffer.append(os.path.join(buffer_path, packages[i].split('.rpm')[0]))
                        os.makedirs(packages_buffer[i])
                if is_url:
                        for package_buffer, link in zip(packages_buffer, links):
                                result = subprocess.run("wget -P {} {}".format(package_buffer, link), stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=True)
                                if result.returncode != 0:
                                        subprocess.run('rm -rf {}/*'.format(buffer_path), shell=True)
                                        print('[Error] {} cannot download'.format(link))
                                        return -4
                else:
                        for package_buffer, package, link in zip(packages_buffer, packages, links):
                                shutil.copy(link, os.path.join(package_buffer, package))

                pattern = r'^(\d+\.\d+\.\d+-\d+\.an\d+)'
                match = re.search(pattern, anolis_kernel_version)
                if match is None:
                        print('[Error] {} is not valid'.format(anolis_kernel_version))
                        subprocess.run('rm -rf {}/*'.format(buffer_path), shell=True)
                        return -5

                for package_buffer, package in zip(packages_buffer, packages):
                        rpm_info = get_rpm_info(os.path.join(package_buffer, package))
                        rpm_match = re.search(pattern, rpm_info['anolis_kernel_version'])
                        if rpm_match is None:
                                print('[Error] {} is not valid'.format(rpm_info['anolis_kernel_version']))
                                subprocess.run('rm -rf {}/*'.format(buffer_path), shell=True)
                                return -3
                        if match.group(1) != rpm_match.group(1):
                                print('[Error] {} is not for anolis'.format(package))
                                subprocess.run('rm -rf {}/*'.format(buffer_path), shell=True)
                                return -3

                        modules_info = []
                        subprocess.run("rpm2cpio {} | cpio -dim -D {}".format(os.path.join(package_buffer, package), package_buffer), shell=True)
                        for root, _, files in os.walk(package_buffer):
                                for file in files:
                                        if file.endswith('.ko'):
                                                module_info = {}
                                                for key in ('name', 'version', 'srcversion'):
                                                        result = subprocess.run("modinfo {} | grep -w '{}:' | awk -F: '{{print($NF);}}' | xargs".format(os.path.join(root, file), key), stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=True)
                                                        if result.returncode != 0:
                                                                subprocess.run('rm -rf {}/*'.format(buffer_path), shell=True)
                                                                print('[Error] {} cannot get modinfo'.format(os.path.join(root, file)))
                                                                return -1
                                                        module_info[key] = result.stdout.decode('utf-8').replace('\n', '')
                                                modules_info.append({'name': module_info['name'], 'version': module_info['version'], 'srcversion': [module_info['srcversion']]})

                        group_index = find_oot_group(oot_baseline, anolis_os_version, match.group(1))
                        if group_index == -1:
                                group = {'anolis_os_version': anolis_os_version, 'anolis_kernel_version': match.group(1), 'oot_modules': list()}
                                oot_baseline.append(group)
                                group_index = len(oot_baseline)-1
                        oot_index = group_query_oot(oot_baseline[group_index], rpm_info['name'], rpm_info['version'])
                        if oot_index == -1:
                                oot = {'oot_name': rpm_info['name'], 'oot_version': rpm_info['version'], 'arch': set(), 'modules': list()}
                                oot['arch'].add(rpm_info['arch'])
                                oot['modules'].extend(modules_info)
                                oot_baseline[group_index]['oot_modules'].append(oot)
                                oot_index = len(oot_baseline[group_index]['oot_modules']) - 1
                        else:
                                oot_baseline[group_index]['oot_modules'][oot_index]['arch'].add(rpm_info['arch'])
                                for module_info in modules_info:
                                        errcode, module_index = oot_query_module(oot_baseline[group_index]['oot_modules'][oot_index], module_info)
                                        if errcode == -1:
                                                oot_baseline[group_index]['oot_modules'][oot_index]['modules'][module_index]['srcversion'].extend(module_info['srcversion'])
                                        elif errcode < -1:
                                                oot_baseline[group_index]['oot_modules'][oot_index]['modules'].append(module_info)

                with open(database, 'w', encoding='utf-8') as file:
                        yaml.dump(oot_baseline, file)
                subprocess.run('rm -rf {}/*'.format(buffer_path), shell=True)

                return 0
                
        except Exception as e:
                traceback.print_exc()
                print(e)
                subprocess.run('rm -rf {}/*'.format(buffer_path), shell=True)

                return -6


def remove(anolis_os_version, anolis_kernel_version, oot_name, oot_version):
        if not os.path.exists(database):
                print('[Error] oot_baseline.yaml does not exist')
                return -1

        with open(database, 'r', encoding='utf-8') as file:
                oot_baseline = yaml.safe_load(file)
        if oot_baseline is None:
                print('[Error] oot_baseline.yaml is empty')
                return -2

        pattern = r'^(\d+\.\d+\.\d+-\d+\.an\d+)'
        match = re.search(pattern, anolis_kernel_version)
        if match:
                group_index = find_oot_group(oot_baseline, anolis_os_version, match.group(1))
                if group_index == -1:
                        print('[Error] OS/Kernel Version Not Match')
                        return -3
                else:
                        oot_index = group_query_oot(oot_baseline[group_index], oot_name, oot_version)
                        if oot_index == -1:
                                print('[Error] OOT Name/Version Not Match')
                                return -4
                        else:
                                oot_baseline[group_index]['oot_modules'].pop(oot_index)
                                with open(database, 'w', encoding='utf-8') as file:
                                        yaml.dump(oot_baseline, file)
                                return 0
        else:
                print('[Error] Kernel Version Format Error]')
                return -5



def group_query_oot(group, oot_name, version):
        for i in range(len(group['oot_modules'])):
                if group['oot_modules'][i]['oot_name'] == oot_name and group['oot_modules'][i]['oot_version'] == version:
                        return i
        return -1

def oot_query_module(oot, module_info):
        for i in range(len(oot['modules'])):
                if oot['modules'][i]['name'] == module_info['name']:
                        if oot['modules'][i]['version'] == module_info['version'] or oot['oot_version'] == module_info['version']:
                                for srcversion_in_baseline in oot['modules'][i]['srcversion']:
                                        for srcversion in module_info['srcversion']:
                                                if srcversion_in_baseline == srcversion:
                                                        return 0, i
                                return -1, i
                        return -2, i
        return -3, -1

def list_koji_packages(url):
        response = requests.get(url)
        if response.status_code != 200:
                print("[Error] Failed to retrieve the page, status code: {response.status_code}")
                return None

        packages = []
        soup = BeautifulSoup(response.text, 'html.parser')
        links = soup.find_all('a')
        for link in links:
                href = link.get('href')
                if href and not href.startswith('?C='):
                        if href.endswith('.rpm') and not href.endswith('.src.rpm'):
                                packages.append(os.path.join(url, href))

        return packages

def get_rpm_info(filepath):
        ts = rpm.TransactionSet()
        if os.path.exists(filepath):
                with open(filepath, 'rb') as f:
                        hdr = ts.hdrFromFdno(f)
        else:
                print('[Error] {} does not exist'.format(filepath))
                return None
        anolis_kernel_version = hdr[rpm.RPMTAG_VERSION].replace('~', '-') + '.' + hdr[rpm.RPMTAG_RELEASE].split('.')[-1]
        oot_name = hdr[rpm.RPMTAG_NAME]
        oot_version = hdr[rpm.RPMTAG_RELEASE].split('~')[0]
        arch = hdr[rpm.RPMTAG_ARCH]

        return {'name': oot_name, 'version': oot_version, 'arch': arch, 'anolis_kernel_version': anolis_kernel_version}

def find_oot_group(oot_baseline, anolis_os_version, anolis_kernel_version):
        if oot_baseline is None:
                return -1
        else:
                for i in range(len(oot_baseline)):
                        group = oot_baseline[i]
                        if group['anolis_os_version'] == anolis_os_version and group['anolis_kernel_version'] == anolis_kernel_version:
                                return i
                return -1

def compare_kernel_version(kernel_version1, kernel_version2):
        pattern = r'^(\d+\.\d+\.\d+-\d+)'
        match1 = re.search(pattern, kernel_version1)
        match2 = re.search(pattern, kernel_version2)
        if match1 and match2:
                return match1.group(1) == match2.group(1)
        else:
                return False

def is_2d_list(var):
        if not isinstance(var, list):
                return False

        return all(isinstance(item, list) for item in var)

if __name__ == '__main__':
        url_ast = "http://8.131.87.1/kojifiles/scratch/ziyi.lgx/task_858399/"
        url_mellanox = "http://8.131.87.1/kojifiles/scratch/ziyi.lgx/task_858393"

        update('Anolis OS 8.9', '5.10.134-17.an8', url_ast)
        update('Anolis OS 8.9', '5.10.134-17.an8', url_mellanox)

        oot = [['mlx5_core', '23.10-3.2.2', '107405FA4A10BC91F57081A'], ['ast', '1.13.1', '107405FA4A10BC91F57081A']]
        errcode, output = query('Anolis OS 8.9', '5.10.134-17.an8.x86_64', 'x86_64', oot)
        if errcode == 0:
                print('[SUCCESS] All Found')
        elif errcode == -1:
                print('[Error] OOT Not Found')
                for item in output:
                        print(item)
        else:
                print('[Error] errcode: {}'.format(errcode))
                for item in output:
                        print(item)

        # remove('Anolis OS 8.9', '5.10.134-17.an8.x86_64', 'kmod-mellanox', '23.10')
