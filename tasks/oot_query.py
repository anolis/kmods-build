#!/usr/bin/env python3

import sys
import os
import ast
import traceback

sys.path.append(os.path.join('/'.join(os.path.dirname(os.path.abspath(__file__)).split('/')[:-1]), 'oot'))
import oot_operations as oot

if __name__ == '__main__':
        if len(sys.argv) != 5:
                print('Usage: {} <anolis_os_version> <anolis_kernel_version> <arch> <oot_list>'.format(sys.argv[0]))
                exit(1)
        try:
                oot_list = ast.literal_eval(sys.argv[4])
                errcode, output = oot.query(sys.argv[1], sys.argv[2], sys.argv[3], oot_list)
                if errcode == 0:
                        print('[SUCCESS] All Found')
                elif errcode > 0:
                        print('[Error] name/version/srcversion Not Match')
                        for item in output:
                                print(item)
                else:
                        print('[Error] Failed to query')
        except Exception as e:
                traceback.print_exc()
                print(e)

# [['mlx5_core', '23.10-3.2.2', '107405FA4A10BC91F57081'], ['mlx5_core', '23.10-3.2.2', '107405FA4A10BC91F57081A'],['ast', '1.13.1', '107405FA4A10BC91F57081A']]
# [['dlb2', '8.2.0', '181449DC7FB6DF7707C51C9']]
# [['dlb2', '', '181449DC7FB6DF7707C51C9']]
# [['txgbevf', '1.3', 'BB372ADFA43F448E93083F3']]
# [['txgbevf', '', 'BB372ADFA43F448E93083F3']]
# [['txgbevfx', '1.3.1', 'BB372ADFA43F448E93083F3']]
