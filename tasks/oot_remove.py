#!/usr/bin/env python3

import sys
import os

sys.path.append(os.path.join('/'.join(os.path.dirname(os.path.abspath(__file__)).split('/')[:-1]), 'oot'))
import oot_operations as oot

if __name__ == '__main__':
        oot.remove(sys.argv[1], sys.argv[2], sys.argv[3], sys.argv[4])