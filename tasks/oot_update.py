#!/usr/bin/env python3

import sys
import os

sys.path.append(os.path.join('/'.join(os.path.dirname(os.path.abspath(__file__)).split('/')[:-1]), 'oot'))
import oot_operations as oot

if __name__ == '__main__':
        if len(sys.argv) != 4:
                print('Usage: {} <oot_info_file> <anolis_os_version> <anolis_kernel_version>'.format(sys.argv[0]))
                exit(1)
        with open(sys.argv[1], 'r', encoding='utf-8') as f:
                lines = f.readlines()
                for line in lines:
                        col = line.split()
                        if len(col) != 3:
                                print('[Error] Invalid oot_info_file: {}'.format(col))
                                exit(1)
                        if col[0] == '1':
                                print('[Info] Updating OOT: {}'.format(col[1]))
                                oot.update(sys.argv[2], sys.argv[3], col[2])
